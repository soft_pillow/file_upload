const express = require('express');
const fileUpload = require('express-fileupload');

const path = require('path');
const app = express();

app.use(fileUpload());

app.get('/', function(req,res){
    res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/upload', function(req, res) {

  if (!req.files)
    return res.status(400).send('No files were uploaded.');

  let appName = req.body.app_name;
  let appVersion = req.body.app_version;
  let sdkVersion = req.body.sdk_version;
  let bundleId = req.body.bundle_id;
  let dSymFile = req.files.dSymFile;
  let appFile = req.files.appFile;
  
  let dSymFilePath = appName+"_"+appVersion+"_"+sdkVersion+"_"+bundleId+".dSym";
  let appFilePath = appName+"_"+appVersion+"_"+sdkVersion+"_"+bundleId+".app";

  dSymFile.mv(path.join(__dirname, './uploads', dSymFilePath), function(err) {
    if (err)
      return res.status(500).send(err);
  });

  appFile.mv(path.join(__dirname, './uploads', appFilePath), function(err) {
    if (err)
      return res.status(500).send(err);
  });

  res.send('File uploaded!');
  
})

app.listen(5000)
